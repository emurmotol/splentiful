package models_test

import (
	"github.com/icrowley/fake"
	"gitlab.com/emurmotol/splentiful/models"
)

// Test user create
func (ms *ModelSuite) TestUserCreate() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	email := fake.EmailAddress()
	password := fake.SimplePassword()

	user := &models.User{
		Email:    email,
		Password: password,
	}

	err = ms.DB.Create(user)
	ms.NoError(err)
	ms.NotZero(user.ID)

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}

// Test user update
func (ms *ModelSuite) TestUserUpdate() {
	user := ms.CreateFakeUser()

	email := fake.EmailAddress()
	password := fake.SimplePassword()

	user.Email = email
	user.Password = password

	err := ms.DB.Update(user)
	ms.NoError(err)
	ms.Equal(email, user.Email)
	ms.Equal(password, user.Password)
}

// Test user delete
func (ms *ModelSuite) TestUserDelete() {
	user := ms.CreateFakeUser()
	err := ms.DB.Destroy(user)
	ms.NoError(err)
}
