package models_test

import (
	"testing"

	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/suite"
	"github.com/icrowley/fake"
	"gitlab.com/emurmotol/splentiful/models"
)

// Used on tests methods
type ModelSuite struct {
	*suite.Model
}

// Entry point for model tests
func TestModelSuite(t *testing.T) {
	model, err := suite.NewModelWithFixtures(packr.NewBox("../fixtures"))
	if err != nil {
		t.Fatal(err)
	}
	as := &ModelSuite{Model: model}
	suite.Run(t, as)
}

// Generate fake user
func (ms *ModelSuite) CreateFakeUser() *models.User {
	user := &models.User{
		Email:    fake.EmailAddress(),
		Password: fake.SimplePassword(),
	}
	err := ms.DB.Create(user)
	ms.NoError(err)
	ms.NotZero(user.ID)
	return user
}
