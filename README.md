# Welcome to Splentiful!

## Application Setup

https://gitlab.com/emurmotol/splentiful/wikis/Application-Setup

## Git Workflow

https://gitlab.com/emurmotol/splentiful/wikis/Git-Workflow

## Database Setup

https://gitlab.com/emurmotol/splentiful/wikis/Database-Setup

## Generate JWT Certificates

https://gitlab.com/emurmotol/splentiful/wikis/Generate-JWT-Certificates

## Running Splentiful

https://gitlab.com/emurmotol/splentiful/wikis/Running-Splentiful

## Docker Setup

https://gitlab.com/emurmotol/splentiful/wikis/Docker-Setup

## Endpoints

https://gitlab.com/emurmotol/splentiful/wikis/Endpoints