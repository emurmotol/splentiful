package actions

import (
	"testing"

	"encoding/json"
	"fmt"
	"net/http"

	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/suite"
	"github.com/icrowley/fake"
	"github.com/markbates/willie"
	"gitlab.com/emurmotol/splentiful/models"
)

// Make fake password available for login
var fakeUserPassword string

// Used on tests methods
type ActionSuite struct {
	*suite.Action
}

// Entry point for actions tests
func TestActionSuite(t *testing.T) {
	action, err := suite.NewActionWithFixtures(App(), packr.NewBox("../fixtures"))
	if err != nil {
		t.Fatal(err)
	}
	as := &ActionSuite{Action: action}
	suite.Run(t, as)
}

// Make a secure request that requires a token
func (as *ActionSuite) Secure(url string, token string) *willie.JSON {
	req := as.JSON(url)
	req.Headers[KeyAuthorization] = fmt.Sprintf("Bearer %s", token)
	return req
}

// Generate a fake user then login
func (as *ActionSuite) ObtainAuthFakeUser() (*models.User, string) {
	user := as.RegisterFakeUser()
	res := as.JSON("/api/v1/login").Post(LoginRequest{
		Email:    user.Email,
		Password: fakeUserPassword,
	})
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	resp := LoginResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	user = resp.User
	as.NotNil(user)
	token := resp.Token
	as.True(len(token) > 0)
	return user, token
}

// Generate fake user
func (as *ActionSuite) RegisterFakeUser() *models.User {
	fakeUserPassword = fake.SimplePassword()
	res := as.JSON("/api/v1/register").Post(RegisterRequest{
		Email:                fake.EmailAddress(),
		Password:             fakeUserPassword,
		PasswordConfirmation: fakeUserPassword,
	})
	as.Equal(http.StatusCreated, res.Code)
	as.NotEmpty(res.Body.String())

	resp := RegisterResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	user := resp.User
	as.NotNil(user)
	return user
}

// Get genesis account
func (as *ActionSuite) GenesisAccount(index int) (accounts.Account, string, *ecdsa.PrivateKey) {
	ks := as.Keystore()
	password := ""

	address, err := envy.MustGet(fmt.Sprintf("ETH_GENESIS_ACCOUNT_%d_ADDRESS", index))
	as.Nil(err)
	account := accounts.Account{Address: common.HexToAddress(address)}

	privateKey, err := envy.MustGet(fmt.Sprintf("ETH_GENESIS_ACCOUNT_%d_PRIVATE_KEY", index))
	as.Nil(err)

	pk, err := crypto.HexToECDSA(privateKey)
	as.Nil(err)
	if !ks.HasAddress(account.Address) {
		account, err = ks.ImportECDSA(pk, password)
		as.Nil(err)
	}
	return account, password, pk
}

// Get keystore
func (as *ActionSuite) Keystore() *keystore.KeyStore {
	path, err := envy.MustGet("ETH_KEY_STORE_PATH")
	as.Nil(err)
	return keystore.NewPlaintextKeyStore(path)
}
