package actions

import (
	"github.com/gobuffalo/buffalo"
)

// Authentication entry point for incomming users
func auth(app *buffalo.App) {
	v1 := app.Group("/api/v1")
	v1.POST("/login", Login)
	v1.POST("/register", Register)
}

// Accessible if authenticated
func secured(app *buffalo.App) {
	v1 := app.Group("/api/v1")
	v1.Use(JWTMiddleware)
	v1.GET("/secured", Secured)
	v1.GET("/users", UserList)
	v1.POST("/users", UserCreate)
	v1.GET("/users/{user_id}", UserShow)
	v1.PUT("/users/{user_id}", UserUpdate)
	v1.DELETE("/users/{user_id}", UserDestroy)
}
