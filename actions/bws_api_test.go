package actions

import (
	"github.com/icrowley/fake"
	"net/http"
)

// Test BWS create wallet
func (as *ActionSuite) TestBWSCreateWallet() {
	res := as.JSON("/bws/api/v2/wallets").Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS add access
func (as *ActionSuite) TestBWSAddAccess() {
	res := as.JSON("/bws/api/v1/copayers/" + fake.Characters()).Put(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS join wallet
func (as *ActionSuite) TestBWSJoinWallet() {
	res := as.JSON("/bws/api/v2/wallets/" + fake.Characters() + "/copayers").Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get status
func (as *ActionSuite) TestBWTGetStatus() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v2/wallets", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get wallet from identifier
func (as *ActionSuite) TestBWSGetWalletFromIdentifier() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/wallets/"+fake.Characters(), token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get preferences
func (as *ActionSuite) TestBWSGetPreferences() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/preferences", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS save preferences
func (as *ActionSuite) TestBWSSavePreferences() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/preferences", token).Put(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get pending transactions
func (as *ActionSuite) TestBWSGetPendingTxs() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS create transaction
func (as *ActionSuite) TestBWSCreateTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v2/txproposals", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS create address
func (as *ActionSuite) TestBWSCreateAddress() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v3/addresses", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get main address
func (as *ActionSuite) TestBWSGetMainAddresses() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/addresses", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get balance
func (as *ActionSuite) TestBWSGetBalance() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/balance", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get fee levels
func (as *ActionSuite) TestBWSGetFeeLevels() {
	res := as.JSON("/bws/api/v2/feelevels").Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test BWS send max info
func (as *ActionSuite) TestBWSGetSendMaxInfo() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/sendmaxinfo", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get user transactions
func (as *ActionSuite) TestBWSGetUtxos() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/utxos", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS brodcast raw transaction
func (as *ActionSuite) TestBWSBroadcastRawTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/broadcast_raw", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS sign transaction
func (as *ActionSuite) TestBWSSignTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters()+"/signatures", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS publish transaction
func (as *ActionSuite) TestBWSPublishTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters()+"/publish", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS broadcast
func (as *ActionSuite) TestBWSBroadcastTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters()+"/broadcast", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS reject transaction
func (as *ActionSuite) TestBWSRejectTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters()+"/rejections", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS remove pending transaction
func (as *ActionSuite) TestBWSRemovePendingTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters(), token).Delete()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get transaction
func (as *ActionSuite) TestBWSGetTx() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txproposals/"+fake.Characters(), token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get transaction history
func (as *ActionSuite) TestBWSGetTxHistory() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txhistory", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS start scan
func (as *ActionSuite) TestBWSStartScan() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/addresses/scan", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS stats
func (as *ActionSuite) TestBWSStats() {
	res := as.JSON("/bws/api/v1/stats").Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test BWS get service version
func (as *ActionSuite) TestBWSGetServiceVersion() {
	res := as.JSON("/bws/api/v1/version").Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test BWS login
func (as *ActionSuite) TestBWSLogin() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/login", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS logout
func (as *ActionSuite) TestBWSLogout() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/login", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get notifications
func (as *ActionSuite) TestBWSGetNotifications() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/notifications", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get transaction note
func (as *ActionSuite) TestBWSGetTxNote() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txnotes/"+fake.Characters(), token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS edit transaction note
func (as *ActionSuite) TestBWSEditTxNote() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txnotes/"+fake.Characters(), token).Put(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get transaction notes
func (as *ActionSuite) TestBWSGetTxNotes() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txnotes", token).Get()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS get fiat rate
func (as *ActionSuite) TestBWSGetFiatRate() {
	res := as.JSON("/bws/api/v1/fiatrates/" + fake.Characters()).Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test BWS push notifications subscribe
func (as *ActionSuite) TestBWSPushNotificationsSubscribe() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/pushnotifications/subscriptions", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS push notifications unsubscribe
func (as *ActionSuite) TestBWSPushNotificationsUnsubscribe() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/pushnotifications/subscriptions/"+fake.Characters(), token).Delete()
	as.Equal(http.StatusNotFound, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS transaction confirmation subscribe
func (as *ActionSuite) TestBWSTxConfirmationSubscribe() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txconfirmations", token).Post(nil)
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}

// Test BWS transaction confirmation unsubscribe
func (as *ActionSuite) TestBWSTxConfirmationUnsubscribe() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/bws/api/v1/txconfirmations/"+fake.Characters(), token).Delete()
	as.Equal(http.StatusBadRequest, res.Code) // TODO: Must be 200
	as.NotEmpty(res.Body.String())
}
