package actions

import (
	"fmt"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"gitlab.com/emurmotol/splentiful/models"
	"strings"
)

// Implements IsValid method
type EmailNotTaken struct {
	c     buffalo.Context
	Name  string
	Field string
}

// Check if an email address is taken or not
func (v *EmailNotTaken) IsValid(errors *validate.Errors) {
	c := v.c
	tx, _ := c.Value(KeyTx).(*pop.Connection)
	user := &models.User{}
	err := tx.Where("email = ?", strings.ToLower(strings.TrimSpace(v.Field))).First(user)

	if err == nil {
		errors.Add(validators.GenerateKey(v.Name), fmt.Sprintf(T.Translate(c, "email.taken"), v.Name))
	}
}
