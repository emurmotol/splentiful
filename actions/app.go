package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo/middleware"
	"github.com/gobuffalo/buffalo/middleware/ssl"
	"github.com/gobuffalo/envy"
	"github.com/unrolled/secure"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gobuffalo/buffalo/middleware/i18n"
	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/x/sessions"
	"github.com/rs/cors"
	"gitlab.com/emurmotol/splentiful/models"
	"gitlab.com/emurmotol/splentiful/utils"
	"net/http"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var (
	ENV = envy.Get("GO_ENV", "development")
	app *buffalo.App
	T   *i18n.Translator
	eth *ethclient.Client
)

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:          ENV,
			SessionStore: sessions.Null{},
			PreWares: []buffalo.PreWare{
				cors.Default().Handler,
			},
			SessionName: "_splentiful_session",
		})

		app.ErrorHandlers[http.StatusBadRequest] = CustomErrHandler()
		app.ErrorHandlers[http.StatusNotFound] = CustomErrHandler()
		app.ErrorHandlers[http.StatusUnprocessableEntity] = CustomErrHandler()
		app.ErrorHandlers[http.StatusInternalServerError] = CustomErrHandler()
		app.ErrorHandlers[http.StatusUnauthorized] = CustomErrHandler()
		app.ErrorHandlers[http.StatusMethodNotAllowed] = CustomErrHandler()

		// Automatically redirect to SSL
		app.Use(forceSSL())

		// Set the request content type to JSON
		app.Use(middleware.SetContentType("application/json"))

		if ENV == "development" {
			app.Use(middleware.ParameterLogger)
		}

		// Wraps each request in a transaction.
		// c.Value(KeyTx).(*pop.PopTransaction)
		// Remove to disable this.
		app.Use(middleware.PopTransaction(models.DB))

		// Setup and use translations
		var err error
		if T, err = i18n.New(packr.NewBox("../locales"), "en-US"); err != nil {
			app.Stop(err)
		}
		app.Use(T.Middleware())

		if eth, err = getEthClient(); err != nil {
			app.Stop(err)
		}

		app.GET("/", Home)
		auth(app)
		secured(app)
		bwsAPI(app)
		bwsAPISecured(app)
	}
	return app
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return ssl.ForceSSL(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}

// Get ethereum client
func getEthClient() (*ethclient.Client, error) {
	url, err := utils.GetEthRawURL(ENV)
	if err != nil {
		return nil, err
	}

	client, err := ethclient.Dial(url)
	if err != nil {
		return nil, err
	}
	return client, nil
}
