package actions

import (
	"encoding/json"
	"github.com/icrowley/fake"
	"net/http"
)

// Test user list
func (as *ActionSuite) TestUserList() {
	count := 5
	for i := 1; i < count; i++ {
		as.RegisterFakeUser()
	}
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/api/v1/users", token).Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	resp := UserListResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	users := resp.Users
	as.Equal(count, len(users))
}

// Test user show
func (as *ActionSuite) TestUserShow() {
	user, token := as.ObtainAuthFakeUser()
	res := as.Secure("/api/v1/users/"+user.ID.String(), token).Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	resp := UserResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	as.NotEmpty(resp.User.ID.String())
}

// Test user create
func (as *ActionSuite) TestUserCreate() {
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/api/v1/users", token).Post(Request{
		"email":    fake.EmailAddress(),
		"password": fake.SimplePassword(),
	})
	as.Equal(http.StatusCreated, res.Code)
	as.NotEmpty(res.Body.String())

	resp := UserResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	as.NotEmpty(resp.User.ID.String())
}

// Test user update
func (as *ActionSuite) TestUserUpdate() {
	user, token := as.ObtainAuthFakeUser()
	newEmail := fake.EmailAddress()
	req := Request{
		"email": newEmail,
	}
	res := as.Secure("/api/v1/users/"+user.ID.String(), token).Put(req)
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	resp := UserResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	as.Equal(newEmail, resp.User.Email)
}

// Test user destroy
func (as *ActionSuite) TestUserDestroy() {
	user, token := as.ObtainAuthFakeUser()
	res := as.Secure("/api/v1/users/"+user.ID.String(), token).Delete()
	as.Equal(http.StatusOK, res.Code)
	as.Contains(res.Body.String(), "success")
}
