package actions

import (
	"net/http"

	"encoding/json"
	"github.com/icrowley/fake"
)

// Test login route
func (as *ActionSuite) TestLogin() {
	// Success
	user := as.RegisterFakeUser()
	res := as.JSON("/api/v1/login").Post(LoginRequest{
		Email:    user.Email,
		Password: fakeUserPassword,
	})
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	resp := LoginResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	user = resp.User
	as.NotNil(user)
	token := resp.Token
	as.True(len(token) > 0)

	// Fail
	req := LoginRequest{
		Email:    fake.EmailAddress(),
		Password: fake.SimplePassword(),
	}
	res = as.JSON("/api/v1/login").Post(req)
	as.Equal(http.StatusUnauthorized, res.Code)
	as.NotEmpty(res.Body.String())

	req.Email = ""
	req.Password = ""
	res = as.JSON("/api/v1/login").Post(req)
	as.Equal(http.StatusUnprocessableEntity, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test register route
func (as *ActionSuite) TestRegister() {
	// Success
	password := fake.SimplePassword()
	res := as.JSON("/api/v1/register").Post(RegisterRequest{
		Email:                fake.EmailAddress(),
		Password:             password,
		PasswordConfirmation: password,
	})
	as.Equal(http.StatusCreated, res.Code)
	as.NotEmpty(res.Body.String())

	resp := RegisterResponse{}
	err := json.Unmarshal(res.Body.Bytes(), &resp)
	as.NoError(err)
	user := resp.User
	as.NotNil(user)

	// Wrong password
	password = fake.SimplePassword()
	req := RegisterRequest{
		Email:                fake.EmailAddress(),
		Password:             password,
		PasswordConfirmation: password + "wrong",
	}

	res = as.JSON("/api/v1/register").Post(req)
	as.Equal(http.StatusUnprocessableEntity, res.Code)
	as.NotEmpty(res.Body.String())

	// Duplicate email
	user = as.RegisterFakeUser()
	req.Email = user.Email
	req.PasswordConfirmation = password
	res = as.JSON("/api/v1/register").Post(req)
	as.Equal(http.StatusUnprocessableEntity, res.Code)
	as.NotEmpty(res.Body.String())

	// Required
	req.Email = ""
	req.Password = ""
	req.PasswordConfirmation = ""
	res = as.JSON("/api/v1/register").Post(req)
	as.Equal(http.StatusUnprocessableEntity, res.Code)
	as.NotEmpty(res.Body.String())
}

// Test secure route for testing purposes only
func (as *ActionSuite) TestSecured() {
	// Success
	_, token := as.ObtainAuthFakeUser()
	res := as.Secure("/api/v1/secured", token).Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())

	// Fail
	res = as.JSON("/api/v1/secured").Get()
	as.Equal(http.StatusUnauthorized, res.Code)
	as.NotEmpty(res.Body.String())

	// Invalid token
	res = as.Secure("/api/v1/secured", fake.Word()).Get()
	as.Equal(http.StatusUnauthorized, res.Code)
	as.NotEmpty(res.Body.String())
}
