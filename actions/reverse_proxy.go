package actions

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/emurmotol/splentiful/utils"
	"net/http"
	"net/http/httputil"
	"net/url"
)

// Reverse proxy for BWS
func ReverseProxy(c buffalo.Context) error {
	target, err := utils.GetBWSRawURL(c.Value(KeyEnv).(string))
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	origin, err := url.Parse(target)
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	proxy := httputil.NewSingleHostReverseProxy(origin)
	proxy.Director = func(req *http.Request) {
		req.Header.Add("X-Forwarded-Host", req.Host)
		req.Header.Add("X-Origin-Host", origin.Host)
		req.URL.Scheme = origin.Scheme
		req.URL.Host = origin.Host
		req.URL.Path = utils.SingleJoiningSlash(origin.Path, req.URL.Path)
	}
	proxy.ModifyResponse = func(resp *http.Response) error {
		return nil
	}
	proxy.ServeHTTP(c.Response(), c.Request())
	return nil
}
