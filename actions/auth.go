package actions

import (
	"database/sql"
	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/pkg/errors"
	"gitlab.com/emurmotol/splentiful/models"
	"gitlab.com/emurmotol/splentiful/utils"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

// Login authenticates a user
func Login(c buffalo.Context) error {
	req := &LoginRequest{}
	if err := c.Bind(req); err != nil {
		return c.Error(http.StatusBadRequest, err)
	}

	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	vErrs := req.Validate(c)
	if vErrs.HasAny() {
		return c.Render(http.StatusUnprocessableEntity, r.JSON(vErrs))
	}

	user := &models.User{}
	email := utils.CleanEmail(req.Email)
	if err := tx.Where("email = ?", email).First(user); err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return c.Error(http.StatusUnauthorized, ErrUserNotFound)
		}
		return c.Error(http.StatusInternalServerError, err)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return c.Error(http.StatusUnauthorized, err)
	}

	expiry, err := utils.GetJWTExpiry()
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	claims := jwt.MapClaims{}
	claims[KeyExp] = expiry
	claims[KeyUserID] = user.ID.String()
	tokenString, err := generateJWTToken(claims, c.Value(KeyEnv).(string))
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	return c.Render(http.StatusOK, r.JSON(LoginResponse{
		User:  user,
		Token: tokenString,
	}))
}

// Register registers a user
func Register(c buffalo.Context) error {
	req := &RegisterRequest{}
	if err := c.Bind(req); err != nil {
		return c.Error(http.StatusBadRequest, err)
	}

	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	vErrs := req.Validate(c)
	if vErrs.HasAny() {
		return c.Render(http.StatusUnprocessableEntity, r.JSON(vErrs))
	}

	user := &models.User{
		Email:    req.Email,
		Password: req.Password,
	}
	err := tx.Create(user)
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	return c.Render(http.StatusCreated, r.JSON(RegisterResponse{
		User: user,
	}))
}

// Secured is a demo logged-in-only area
func Secured(c buffalo.Context) error {
	claims := c.Value(KeyClaims).(jwt.MapClaims)

	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	user := &models.User{}
	if tx.Find(user, claims[KeyUserID]) != nil {
		return c.Error(http.StatusNotFound, ErrUserNotFound)
	}

	res := Response{}
	res[KeyClaims] = claims
	return c.Render(http.StatusOK, r.JSON(res))
}
