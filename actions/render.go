package actions

import (
	"github.com/gobuffalo/buffalo/render"
)

// Used to hold and make render accessible to actions
var r *render.Engine

// Initialize render engine value
func init() {
	r = render.New(render.Options{})
}
