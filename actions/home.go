package actions

import (
	"net/http"

	"github.com/gobuffalo/buffalo"
)

//  Default handler to serve up
func Home(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.JSON(Response{
		KeyMessage: T.Translate(c, "welcome.splentiful"),
	}))
}
