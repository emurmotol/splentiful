package actions

import (
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/icrowley/fake"
	"golang.org/x/net/context"
	"math/big"
)

func (as *ActionSuite) TestEthAPI() {
	ctx := context.Background()
	ks := as.Keystore()

	sender, password, _ := as.GenesisAccount(0)
	receiver, _, _ := as.GenesisAccount(1)

	data := []byte(fake.Sentence())
	value := big.NewInt(1000000000000000000)
	gasPrice, err := eth.SuggestGasPrice(ctx)
	as.Nil(err)

	msg := ethereum.CallMsg{
		From:     sender.Address,
		To:       &receiver.Address,
		GasPrice: gasPrice,
		Value:    value,
		Data:     data,
	}
	gasLimit, err := eth.EstimateGas(ctx, msg)
	as.Nil(err)

	nonce, err := eth.PendingNonceAt(ctx, sender.Address)
	as.Nil(err)

	tx := types.NewTransaction(nonce, receiver.Address, value, gasLimit, gasPrice, data)
	networkID, err := eth.NetworkID(ctx)
	as.Nil(err)
	signedTx, err := ks.SignTxWithPassphrase(sender, password, tx, networkID)
	as.Nil(err)

	err = eth.SendTransaction(ctx, signedTx)
	as.Nil(err)
	as.NotEmpty(signedTx.Hash().Hex())

	header, err := eth.HeaderByNumber(ctx, nil)
	as.Nil(err)

	block, err := eth.BlockByNumber(ctx, header.Number)
	as.Nil(err)
	as.Equal(0, len(block.Transactions()))
}
