package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/pkg/errors"
)

var (
	// Returned if no database transaction was found
	ErrTransactionNotFound = errors.New("transaction not found")
	// Returned if a user was not found in database
	ErrUserNotFound = errors.New("user not found")
	// Returned when the token provided is invalid
	ErrJWTTokenInvalid = errors.New("token invalid")
	// Returned if no token is supplied in the request.
	ErrJWTNoToken = errors.New("token not found in request")
	// Returned if the token sign method in the request
	// Does not match the signing method used
	ErrJWTBadSigningMethod = errors.New("unexpected signing method")
)

// Custom error handler for error responses
func CustomErrHandler() buffalo.ErrorHandler {
	return func(status int, err error, c buffalo.Context) error {
		return c.Render(status, r.JSON(Response{
			KeyError: err.Error(),
		}))
	}
}
