package actions

import "github.com/gobuffalo/buffalo"

// Public exposed BWS routes
func bwsAPI(app *buffalo.App) {
	v1 := app.Group("/bws/api/v1")
	v1.PUT("/copayers/{id}", BWSAddAccess)
	v1.GET("/stats", BWSStats)
	v1.GET("/version", BWSGetServiceVersion)
	v1.GET("/fiatrates/{code}", BWSGetFiatRate)

	v2 := app.Group("/bws/api/v2")
	v2.POST("/wallets", BWSCreateWallet)
	v2.POST("/wallets/{id}/copayers", BWSJoinWallet)
	v2.GET("/feelevels", BWSGetFeeLevels)
}

// Accessible if authenticated
func bwsAPISecured(app *buffalo.App) {
	v1 := app.Group("/bws/api/v1")
	v1.Use(JWTMiddleware)
	v1.GET("/wallets/{identifier}", BWSGetWalletFromIdentifier)
	v1.GET("/preferences", BWSGetPreferences)
	v1.PUT("/preferences", BWSSavePreferences)
	v1.GET("/txproposals", BWSGetPendingTxs)
	v1.GET("/addresses", BWSGetMainAddresses)
	v1.GET("/balance", BWSGetBalance)
	v1.GET("/sendmaxinfo", BWSGetSendMaxInfo)
	v1.GET("/utxos", BWSGetUtxos)
	v1.POST("/broadcast_raw", BWSBroadcastRawTx)
	v1.POST("/txproposals/{id}/signatures", BWSSignTx)
	v1.POST("/txproposals/{id}/publish", BWSPublishTx)
	v1.POST("/txproposals/{id}/broadcast", BWSBroadcastTx)
	v1.POST("/txproposals/{id}/rejections", BWSRejectTx)
	v1.DELETE("/txproposals/{id}", BWSRemovePendingTx)
	v1.GET("/txproposals/{id}", BWSGetTx)
	v1.GET("/txhistory", BWSGetTxHistory)
	v1.POST("/addresses/scan", BWSStartScan)
	v1.POST("/login", BWSLogin)
	v1.POST("/logout", BWSLogout)
	v1.GET("/notifications", BWSGetNotifications)
	v1.GET("/txnotes/{txid}", BWSGetTxNote)
	v1.PUT("/txnotes/{txid}", BWSEditTxNote)
	v1.GET("/txnotes", BWSGetTxNotes)
	v1.POST("/pushnotifications/subscriptions", BWSPushNotificationsSubscribe)
	v1.POST("/txconfirmations", BWSTxConfirmationSubscribe)
	v1.DELETE("/txconfirmations/{txid}", BWSTxConfirmationUnsubscribe)

	v2 := app.Group("/bws/api/v2")
	v2.Use(JWTMiddleware)
	v2.GET("/wallets", BWTGetStatus)
	v2.POST("/txproposals", BWSCreateTx)
	v2.DELETE("/pushnotifications/subscriptions/{token}", BWSPushNotificationsUnsubscribe)

	v3 := app.Group("/bws/api/v3")
	v3.Use(JWTMiddleware)
	v3.POST("/addresses", BWSCreateAddress)
}
