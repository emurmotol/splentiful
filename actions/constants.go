package actions

// Constant values
const (
	KeyStatus        = "status"
	KeyToken         = "token"
	KeyMessage       = "message"
	KeyUser          = "user"
	KeyUsers         = "users"
	KeyErrors        = "errors"
	KeyError         = "error"
	KeyPagination    = "pagination"
	KeyClaims        = "claims"
	KeyAuthorization = "Authorization"
	KeyTx            = "tx"
	KeyExp           = "exp"
	KeyUserID        = "user_id"
	KeyEnv           = "env"
)
