package actions

// Generic request type
type Request map[string]interface{}
