package actions

import (
	"net/http"
)

// Test home route
func (as *ActionSuite) TestHome() {
	res := as.HTML("/").Get()
	as.Equal(http.StatusOK, res.Code)
	as.NotEmpty(res.Body.String())
}
