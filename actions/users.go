package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"gitlab.com/emurmotol/splentiful/models"
	"net/http"
)

// Gets all Users
func UserList(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	users := models.Users{}

	// Paginate results. Params "page" and "per_page" control pagination.
	// Default values are "page=1" and "per_page=20".
	q := tx.PaginateFromParams(c.Params())

	// Retrieve all Users from the DB
	if err := q.All(&users); err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	return c.Render(http.StatusOK, r.JSON(UserListResponse{
		Pagination: q.Paginator,
		Users:      users,
	}))
}

// Gets the data for one User
func UserShow(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	// Allocate an empty User
	user := &models.User{}

	// To find the User the parameter user_id is used.
	if tx.Find(user, c.Param(KeyUserID)) != nil {
		return c.Error(http.StatusNotFound, ErrUserNotFound)
	}

	return c.Render(http.StatusOK, r.JSON(UserResponse{
		User: user,
	}))
}

// Adds a User to the DB
func UserCreate(c buffalo.Context) error {
	// Allocate an empty User
	user := &models.User{}

	if err := c.Bind(user); err != nil {
		return c.Error(http.StatusBadRequest, err)
	}

	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	vErrs, err := tx.ValidateAndCreate(user)
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	if vErrs.HasAny() {
		return c.Render(http.StatusUnprocessableEntity, r.JSON(vErrs))
	}

	return c.Render(http.StatusCreated, r.JSON(UserResponse{
		User: user,
	}))
}

// Changes a User in the DB
func UserUpdate(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	// Allocate an empty User
	user := &models.User{}

	if tx.Find(user, c.Param(KeyUserID)) != nil {
		return c.Error(http.StatusNotFound, ErrUserNotFound)
	}

	if err := c.Bind(user); err != nil {
		return c.Error(http.StatusBadRequest, err)
	}

	vErrs, err := tx.ValidateAndUpdate(user)
	if err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	if vErrs.HasAny() {
		return c.Render(http.StatusUnprocessableEntity, r.JSON(vErrs))
	}

	return c.Render(http.StatusOK, r.JSON(UserResponse{
		User: user,
	}))
}

// Deletes a User from the DB
func UserDestroy(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value(KeyTx).(*pop.Connection)
	if !ok {
		return c.Error(http.StatusInternalServerError, ErrTransactionNotFound)
	}

	// Allocate an empty User
	user := &models.User{}

	// To find the User the parameter user_id is used.
	if tx.Find(user, c.Param(KeyUserID)) != nil {
		return c.Error(http.StatusNotFound, ErrUserNotFound)
	}

	if err := tx.Destroy(user); err != nil {
		return c.Error(http.StatusInternalServerError, err)
	}

	return c.Render(http.StatusOK, r.JSON(Response{
		KeyMessage: T.Translate(c, "user.destroy.success"),
	}))
}
