package actions

import (
	"github.com/gobuffalo/pop"
	"gitlab.com/emurmotol/splentiful/models"
)

// Used to bind user list response
type UserListResponse struct {
	Pagination *pop.Paginator `json:"pagination"`
	Users      models.Users   `json:"users"`
}

// Used to bind user response
type UserResponse struct {
	User *models.User `json:"user"`
}
