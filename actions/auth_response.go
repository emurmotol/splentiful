package actions

import "gitlab.com/emurmotol/splentiful/models"

// Used to bind login response
type LoginResponse struct {
	User  *models.User `json:"user"`
	Token string       `json:"token"`
}

// Used to bind register response
type RegisterResponse struct {
	User *models.User `json:"user"`
}
