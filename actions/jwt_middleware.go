package actions

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"io/ioutil"
	"net/http"
	"strings"
)

// Filters request making sure JWT token is in header and valid
func JWTMiddleware(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		// get key for validation
		key, err := getKeyRSA(c.Value(KeyEnv).(string))
		if err != nil {
			return c.Error(http.StatusInternalServerError, err)
		}
		// get Authorisation header value
		authString := c.Request().Header.Get(KeyAuthorization)

		tokenString, err := getJWTToken(authString)
		// if error on getting the token, return with status unauthorized
		if err != nil {
			return c.Error(http.StatusUnauthorized, err)
		}

		// validating and parsing the tokenString
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			// Validating if algorithm used for signing is same as the algorithm in token
			if token.Method.Alg() != jwt.SigningMethodRS256.Alg() {
				return nil, ErrJWTBadSigningMethod
			}
			return key, nil
		})
		// if error validating jwt token, return with status unauthorized
		if err != nil {
			return c.Error(http.StatusUnauthorized, err)
		}

		// set the claims as context parameter.
		// so that the actions can use the claims from jwt token
		c.Set(KeyClaims, token.Claims)
		// calling next handler
		return next(c)
	}
}

// Gets the public key file location from env and returns rsa.PublicKey
func getKeyRSA(env string) (interface{}, error) {
	publicKey, err := envy.MustGet("JWT_PUBLIC_KEY")
	if err != nil {
		return nil, err
	}

	if env == "test" {
		publicKey = fmt.Sprintf("../%s", publicKey)
	}

	key, err := ioutil.ReadFile(publicKey)
	if err != nil {
		return nil, err
	}
	return jwt.ParseRSAPublicKeyFromPEM(key)
}

// Gets the token from the Authorisation header
// Removes the Bearer part from the authorisation header value.
// Returns No token error if Token is not found
// Returns Token Invalid error if the token value cannot be obtained by removing `Bearer `
func getJWTToken(authString string) (string, error) {
	if authString == "" {
		return "", ErrJWTNoToken
	}
	splitToken := strings.Split(authString, "Bearer ")
	if len(splitToken) != 2 {
		return "", ErrJWTTokenInvalid
	}
	tokenString := splitToken[1]
	return tokenString, nil
}

// Generate new JWT token
func generateJWTToken(claims jwt.MapClaims, env string) (string, error) {
	privateKey, err := envy.MustGet("JWT_PRIVATE_KEY")
	if err != nil {
		return "", err
	}
	if env == "test" {
		privateKey = fmt.Sprintf("../%s", privateKey)
	}
	key, err := ioutil.ReadFile(privateKey)
	if err != nil {
		return "", err
	}
	parsedKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		return "", err
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	tokenString, err := token.SignedString(parsedKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
