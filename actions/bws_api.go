package actions

import (
	"github.com/gobuffalo/buffalo"
)

// Proxy for BWS createWallet
func BWSCreateWallet(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS addAccess
func BWSAddAccess(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS joinWallet
func BWSJoinWallet(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getStatus
func BWTGetStatus(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getWalletFromIdentifier
func BWSGetWalletFromIdentifier(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getPreferences
func BWSGetPreferences(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS savePreferences
func BWSSavePreferences(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getPendingTxs
func BWSGetPendingTxs(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS createTx
func BWSCreateTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS createAddress
func BWSCreateAddress(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getMainAddresses
func BWSGetMainAddresses(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getBalance
func BWSGetBalance(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getFeeLevels
func BWSGetFeeLevels(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getSendMaxInfo
func BWSGetSendMaxInfo(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getUtxos
func BWSGetUtxos(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS broadcastRawTx
func BWSBroadcastRawTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS signTx
func BWSSignTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS publishTx
func BWSPublishTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS broadcastTx
func BWSBroadcastTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS rejectTx
func BWSRejectTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS rejectTx
func BWSRemovePendingTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getTx
func BWSGetTx(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getTxHistory
func BWSGetTxHistory(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS startScan
func BWSStartScan(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS stats
func BWSStats(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getServiceVersion
func BWSGetServiceVersion(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS login
func BWSLogin(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS logout
func BWSLogout(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getNotifications
func BWSGetNotifications(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getTxNote
func BWSGetTxNote(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS editTxNote
func BWSEditTxNote(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getTxNotes
func BWSGetTxNotes(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS getFiatRate
func BWSGetFiatRate(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS pushNotificationsSubscribe
func BWSPushNotificationsSubscribe(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS pushNotificationsUnsubscribe
func BWSPushNotificationsUnsubscribe(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS txConfirmationSubscribe
func BWSTxConfirmationSubscribe(c buffalo.Context) error {
	return ReverseProxy(c)
}

// Proxy for BWS txConfirmationUnsubscribe
func BWSTxConfirmationUnsubscribe(c buffalo.Context) error {
	return ReverseProxy(c)
}
