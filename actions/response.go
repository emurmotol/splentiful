package actions

// Generic response type
type Response map[string]interface{}
