package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Used to bind login request data
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Used to bind register request data
type RegisterRequest struct {
	Email                string `json:"email"`
	Password             string `json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
}

// Validate login request
func (req *LoginRequest) Validate(c buffalo.Context) *validate.Errors {
	return validate.Validate(
		&validators.EmailIsPresent{Field: req.Email, Name: "Email"},
		&validators.StringIsPresent{Field: req.Password, Name: "Password"},
	)
}

// Validate register request
func (req *RegisterRequest) Validate(c buffalo.Context) *validate.Errors {
	return validate.Validate(
		&validators.EmailIsPresent{Field: req.Email, Name: "Email"},
		&validators.StringIsPresent{Field: req.Password, Name: "Password"},
		&validators.StringIsPresent{Field: req.PasswordConfirmation, Name: "PasswordConfirmation"},
		&validators.StringsMatch{Name: "Password", Field: req.Password, Field2: req.PasswordConfirmation, Message: T.Translate(c, "password.confirm.not.match")},
		&EmailNotTaken{c: c, Field: req.Email, Name: "Email"},
	)
}
