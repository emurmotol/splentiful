package utils

import "strings"

func CleanEmail(email string) string {
	return strings.ToLower(strings.TrimSpace(email))
}
