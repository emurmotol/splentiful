package utils

import (
	"github.com/gobuffalo/envy"
	"strconv"
	"time"
)

// Gets the BWS URL from environment
func GetBWSRawURL(env string) (string, error) {
	var url string
	var err error
	switch env {
	case "test":
		url, err = envy.MustGet("TEST_BWS_RAW_URL")
		if err != nil {
			return "", err
		}
	case "development":
		url, err = envy.MustGet("DEVELOPMENT_BWS_RAW_URL")
		if err != nil {
			return "", err
		}
	case "production":
		url, err = envy.MustGet("PRODUCTION_BWS_RAW_URL")
		if err != nil {
			return "", err
		}
	}
	return url, nil
}

// Gets the expiration duration for a token
func GetJWTExpiry() (int64, error) {
	value, err := envy.MustGet("JWT_EXPIRE_IN_SECONDS")
	if err != nil {
		return int64(0), err
	}

	seconds, err := strconv.Atoi(value)
	if err != nil {
		return int64(0), err
	}
	return time.Now().Add(time.Second * time.Duration(seconds)).Unix(), nil
}

// Gets ethereum RPC server address
func GetEthRawURL(env string) (string, error) {
	var url string
	var err error
	switch env {
	case "test":
		url, err = envy.MustGet("TEST_ETH_RAW_URL")
		if err != nil {
			return "", err
		}
	case "development":
		url, err = envy.MustGet("DEVELOPMENT_ETH_RAW_URL")
		if err != nil {
			return "", err
		}
	case "production":
		url, err = envy.MustGet("PRODUCTION_ETH_RAW_URL")
		if err != nil {
			return "", err
		}
	}
	return url, nil
}
