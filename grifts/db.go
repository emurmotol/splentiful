package grifts

import (
	"github.com/markbates/grift/grift"
)

// Task function for db command
var _ = grift.Namespace("db", func() {

	grift.Desc("seed", "Seeds a database")
	grift.Add("seed", func(c *grift.Context) error {
		// Add DB seeding stuff here
		return nil
	})
})
