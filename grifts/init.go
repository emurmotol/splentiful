package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/emurmotol/splentiful/actions"
)

// Initialize grifts
func init() {
	buffalo.Grifts(actions.App())
}
